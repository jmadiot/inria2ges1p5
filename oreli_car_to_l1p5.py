#!/usr/bin/python3


import csv
import sys
import itertools
from datetime import datetime, timedelta

# see also oreli_car_readme.txt
in_filename = '../terrestre.csv'
out_filename = 'car-labos1p5.tsv'


with open(in_filename, encoding='utf8') as file:
    rows = list(csv.DictReader(file, delimiter=','))

# # 'worldcities.csv' obtained from https://simplemaps.com/data/world-cities
# # ---> careful, many cities are in the US
# in fact, better not to use this, most cities are in france anyway, so
# some of the manual geocoding below is just checking that it returns FR
# on the non-manually-classified-as-non-french cities
#
# cities_by_name = {}
# with open('worldcities.csv', encoding='utf8') as file:
#     cities = list(csv.DictReader(file, delimiter=','))
#     for row in cities:
#         cities_by_name[row['city_ascii'].lower()] = row

# manual entries for country codes (sometimes with a new name? not sure if useful)
# city,country_code,fix_city_name
manual_geocode = {}
with open('cities-countries.csv', encoding='utf8') as file:
    cities = list(csv.DictReader(file, delimiter=','))
    for row in cities:
        manual_geocode[row['city'].lower()] = row

nrows = len(rows)

def same_mission(row1, row2):
    if row1 is None: return False
    agent = lambda row: row["Matricule de l'agent Mission"]
    itinerary = lambda row: row["Itinéraire Mission"]
    if agent(row1) != agent(row2) or itinerary(row1) != itinerary(row2):
        return False
    date = lambda row: datetime.strptime(row["Date départ Mission"], '%d/%m/%Y')
    return abs(date(row1) - date(row2)) <= timedelta(days = 21)

# add mission numbers
id = 0
last_row = None
for row in rows:
    if not same_mission(last_row, row):
        id += 1
    row['id'] = id
    last_row = row

# whether the itinerary has the form "X - Y - X"
def standard_XYX(row):
    it = row["Itinéraire Mission"].split(' - ')
    return len(it) == 3 and it[0] == it[2]

# Group by 1 or 2 to make two ways missions when possible
# (some processing is needed anyway not to have twice "paris->rennes"
# when one should be "rennes->paris")
groups = []
for row in rows:
    if len(groups) == 0 or len(groups[-1]) >= 2:
        groups.append([row])
    else:
        lastgroup = groups[-1]
        assert len(lastgroup) == 1
        if lastgroup[0]['id'] == row['id'] and standard_XYX(row):
            lastgroup.append(row)
        else:
            groups.append([row])

assert nrows == sum(len(g) for g in groups)

for g in groups:
    if len(g) == 2:
        # sanity check (but 0 km is frequent here)
        km = 'Kilométrage itinéraire Mission'
        assert g[0][km] == g[1][km]
        g[0]['twoways'] = True
        g.pop() # remove the second row
    else:
        assert(len(g) == 1)
        g[0]['twoways'] = False

rows = [g[0] for g in groups]

# check the number of twoways is consistent with the (original) nb of rows
assert nrows == sum(2 if row['twoways'] else 1 for row in rows)

# returns city, country_code
# fix the city name
# (londres -> london, paris XXe arr -> paris, etc
# (might be unnecessary, I was just trying to clean up the dataset)
def geocode(name):
    name = name.lower()
    if name == 'paris' or 'arrondissement' in name and 'paris' in name:
        return 'Paris', 'fr'
    if name in manual_geocode:
        info = manual_geocode[name]
        if info['fix_city_name']:
            name = info['fix_city_name']
        return name.capitalize(), info['country_code']
    # # see comment about 'worldcities.csv' for when this is useful and when it is harmful
    # if name in cities_by_name:
    #     print(f"{name},{cities_by_name[name]['iso2'].lower()},")
    #     return name, cities_by_name[name]['iso2'].lower()
    raise Exception("Error: country code unknown for '{name}'")

def nb_people_in_car(row):
    s = row['Transport principal Mission']
    d = {
        'VP' : 1, # Véhicule personnel
        'VL' : 1, # Véhicule de location
        'CV' : 2, # covoiturage
        'TA' : 1, # Taxi
    }
    if s in d:
        return d[s]
    return Exception('unknown transport type:', s)

oreli_out = []
for row in rows:
    src, src_code = geocode(row["Itinéraire Mission"].split(' - ')[0])
    dst, dst_code = geocode(row["Itinéraire Mission"].split(' - ')[1])
    l1p5_row = {
        '# mission': row['id'],
        'Date de départ': row['Date départ Mission'],
        'Ville de départ': src,
        'Pays de départ': src_code,
        'Ville de destination': dst,
        'Pays de destination': dst_code,
        'Mode de déplacement': 'Voiture',
        'Nb de personnes dans la voiture': nb_people_in_car(row),
        'Aller Retour (OUI si identiques, NON si différents)': "OUI" if row['twoways'] else "NON",
        'Motif du déplacement (optionnel)': '',
        'Statut de l\'agent (optionnel)': ''
    }
    oreli_out.append(l1p5_row)

if len(oreli_out) > 0:
    with open(out_filename, 'w', newline='', encoding='utf8') as csvfile:
        fieldnames = oreli_out[0].keys()
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames, delimiter='\t')
        writer.writeheader()
        for row in oreli_out:
            writer.writerow(row)
    print(len(oreli_out), 'entries written to', out_filename)
